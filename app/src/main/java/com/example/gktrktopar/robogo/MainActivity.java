package com.example.gktrktopar.robogo;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Build;
import android.preference.Preference;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.UUID;

import io.github.controlwear.virtual.joystick.android.JoystickView;

import static java.lang.String.valueOf;

public class MainActivity extends AppCompatActivity {
    private TextView info;
    private Spinner kdspinner;
    private Spinner kpspinner;
    private Spinner kispinner;
    private Spinner masspinner;
    private Double[] kd_values=new Double[52];
    private Double[] kp_values=new Double[501];
    private Double[] ki_values=new Double[2001];
    private Double[] mas_values=new Double[121];
    private ArrayAdapter<Double> kdadapter;
    private ArrayAdapter<Double> kiadapter;
    private ArrayAdapter<Double> kpadapter;
    private ArrayAdapter<Double> masadapter;
    private BluetoothAdapter mBluetoothAdapter;
    BluetoothSocket btSocket = null;
    double before=0.6;
    private BooVariable isBtConnected;
    String device_address = "00:21:13:00:B9:B1";
    static final UUID myUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    /*Buttons*/
    Button onOfButton;
    Button autotuningbutton;
    SeekBar kp_value;
    SeekBar ki_value;
    SeekBar kd_value;
    SeekBar mas_value;

    Boolean on_off=true;// device is on
    Boolean auto_tuning=true;//device is not auto tuning mode
    private ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        isBtConnected = new BooVariable();
        isBtConnected.setBoo(false);

        kdspinner = (Spinner) findViewById(R.id.spinnerforkd);
        kpspinner = (Spinner) findViewById(R.id.spinnerforkp);
        kispinner = (Spinner) findViewById(R.id.spinnerforki);
        masspinner = (Spinner) findViewById(R.id.spinnerformas);
        double firstvalue=0;
        for (int i=0;i<501;i++){
            String d="";
            String x=Double.toString(firstvalue);
            String[] arr = x.split("\\.");
            if (arr.length>1) {
                d = arr[0] + "." + arr[1].substring(0, 1);
                kp_values[i] = Double.parseDouble(d);
            }



            firstvalue+=0.5;
        }
        firstvalue=0;
        for (int i=0;i<2001;i++){
            String d="";
            String x=Double.toString(firstvalue);
            String[] arr = x.split("\\.");
            if (arr.length>1) {
                d = arr[0] + "." + arr[1].substring(0, 1);
                ki_values[i] = Double.parseDouble(d);
            }



            firstvalue+=0.5;
        }
        firstvalue=0;
        for (int i=0;i<kd_values.length;i++){
            String d="";
            String x=Double.toString(firstvalue);
            String[] arr = x.split("\\.");
            if (arr.length>1) {
                 d = arr[0] + "." + arr[1].substring(0, 1);
                kd_values[i] = Double.parseDouble(d);
            }



            firstvalue+=0.1;
        }
        firstvalue=0;
        for (int i=0;i<121;i++){
            String d="";
            String x=Double.toString(firstvalue);
            String[] arr = x.split("\\.");
            if (arr.length>1) {
                d = arr[0] + "." + arr[1].substring(0, 1);
                mas_values[i] = Double.parseDouble(d);
            }



            firstvalue+=0.5;
        }

        //Spinner'lar için adapterleri hazırlıyoruz.
        kpadapter = new ArrayAdapter<Double>(this, android.R.layout.simple_spinner_item, kp_values);
        kpadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        kpspinner.setAdapter(kpadapter);

        kdadapter = new ArrayAdapter<Double>(this, android.R.layout.simple_spinner_item, kd_values);
        kdadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        kdspinner.setAdapter(kdadapter);

        kiadapter = new ArrayAdapter<Double>(this, android.R.layout.simple_spinner_item, ki_values);
        kiadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        kispinner.setAdapter(kiadapter);

        masadapter = new ArrayAdapter<Double>(this, android.R.layout.simple_spinner_item, mas_values);
        masadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        masspinner.setAdapter(masadapter);

        kd_value = (SeekBar) findViewById(R.id.progressBarKd);
        ki_value = (SeekBar) findViewById(R.id.progressBarKi);
        kp_value = (SeekBar) findViewById(R.id.progressBarKp);
        mas_value = (SeekBar) findViewById(R.id.progressBarMAS);

        onOfButton = (Button) findViewById(R.id.OnOffButton);
        autotuningbutton = (Button) findViewById(R.id.autoButton);
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        mBluetoothAdapter.startDiscovery();
        if (mBluetoothAdapter == null) {
            //Show a mensag. that the device has no bluetooth adapter
            Toast.makeText(getApplicationContext(), "Bluetooth Device Not Available at Your Phone...", Toast.LENGTH_LONG).show();

            finish();
        } else if (!mBluetoothAdapter.isEnabled()) {
            Intent turnBTon = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(turnBTon, 1);
        }
        reset_values();
        new ConnectBT().execute();

        JoystickView joystick = (JoystickView) findViewById(R.id.joystick);

        kdspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                Double selectedItemdouble = (Double) parentView.getItemAtPosition(position);
                long longValue = Math.round(selectedItemdouble);
                int intValue = (int) longValue;
                kd_value.setProgress(intValue);
                String selectedItemText = String.valueOf(selectedItemdouble);
                if (isBtConnected.isBoo()){
                      try {
                           btSocket.getOutputStream().write(("Kd" + selectedItemText + "#").toString().getBytes());

                        } catch (IOException e) {

                        }
                    }
                    else {

                        isBtConnected.setBoo(false);



                    }
                }


            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
        kispinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                Double selectedItemdouble = (Double) parentView.getItemAtPosition(position);
                long longValue = Math.round(selectedItemdouble);
                int intValue = (int) longValue;
                ki_value.setProgress(intValue);
                String selectedItemText = String.valueOf(selectedItemdouble);
                if (isBtConnected.isBoo()){
                    try {
                        btSocket.getOutputStream().write(("Ki" + selectedItemText + "#").toString().getBytes());

                    } catch (IOException e) {

                    }
                }
                else {

                    isBtConnected.setBoo(false);



                }
            }


            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
        kpspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                Double selectedItemdouble = (Double) parentView.getItemAtPosition(position);
                long longValue = Math.round(selectedItemdouble);
                int intValue = (int) longValue;
                kp_value.setProgress(intValue);
                String selectedItemText = String.valueOf(selectedItemdouble);
                if (isBtConnected.isBoo()){
                    try {
                        btSocket.getOutputStream().write(("Kp" + selectedItemText + "#").toString().getBytes());

                    } catch (IOException e) {

                    }
                }
                else {

                    isBtConnected.setBoo(false);



                }
            }


            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
        masspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                Double selectedItemdouble = (Double) parentView.getItemAtPosition(position);
                long longValue = Math.round(selectedItemdouble);
                int intValue = (int) longValue;
                mas_value.setProgress(intValue);
                String selectedItemText = String.valueOf(selectedItemdouble);
                if (isBtConnected.isBoo()){
                    try {
                        btSocket.getOutputStream().write(("AS" + selectedItemText + "#").toString().getBytes());

                    } catch (IOException e) {

                    }
                }
                else {

                    isBtConnected.setBoo(false);



                }
            }


            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
            joystick.setOnMoveListener(new JoystickView.OnMoveListener() {
                @Override
                public void onMove(int angle, int strength) {
                    if (isBtConnected.isBoo()) {
                        if (angle >= 30 && angle <= 120) { // move forth
                            try {
                                btSocket.getOutputStream().write("mv2#".getBytes());



                            } catch (IOException e) {
                                e.printStackTrace();
                                msg("Connection Error...");
                                isBtConnected.setBoo(false);
                                try_to_connect_again();
                            }
                        } else if (angle >= 210 && angle <= 300) { // move back
                            try {
                                btSocket.getOutputStream().write("mv1#".getBytes());

                            } catch (IOException e) {
                                e.printStackTrace();
                                msg("Connection Error...");
                                isBtConnected.setBoo(false);
                                try_to_connect_again();


                            }
                        } else if (angle > 120 && angle < 210) { // move left
                            try {
                                btSocket.getOutputStream().write("mv4#".getBytes());
                            //    btSocket.getOutputStream().write("mv0#".getBytes());

                            } catch (IOException e) {
                                e.printStackTrace();
                                msg("Connection Error...");
                                isBtConnected.setBoo(false);
                                try_to_connect_again();


                            }
                        } else if (angle < 30 && angle > 300) { // move right
                            try {
                                btSocket.getOutputStream().write("mv3#".getBytes());


                            } catch (IOException e) {
                                e.printStackTrace();
                                msg("Connection Error...");
                                isBtConnected.setBoo(false);
                                try_to_connect_again();


                            }
                        }
                    }
                }
            });





        onOfButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isBtConnected.isBoo()) {
                    if (on_off) { // device is on
                        try {
                            btSocket.getOutputStream().write("on0#".getBytes());

                            onOfButton.setBackgroundColor(getResources().getColor(R.color.red));

                            on_off = false;
                        } catch (IOException e) {
                            e.printStackTrace();
                            msg("Connection Error...");
                            isBtConnected.setBoo(false);
                            try_to_connect_again();


                        }
                    } else {
                        try {
                            btSocket.getOutputStream().write("on1#".getBytes());
                            // btSocket.getOutputStream().write(String.valueOf(0).getBytes());
                            onOfButton.setBackgroundColor(getResources().getColor(R.color.green));

                            on_off = true;
                        } catch (IOException e) {
                            e.printStackTrace();
                            msg("Connection Error...");
                            isBtConnected.setBoo(false);
                            try_to_connect_again();

                        }
                    }
                }
            }
        });


        autotuningbutton.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View v) {
                if (isBtConnected.isBoo()) {
                    if (auto_tuning) { // device is automatic tuning
                        try {
                            btSocket.getOutputStream().write("tn1#".toString().getBytes());
                            //   btSocket.getOutputStream().write(String.valueOf(1).getBytes());
                            autotuningbutton.setBackgroundColor(getResources().getColor(R.color.red));
                            autotuningbutton.setText("MANUAL TUNING");
                            kd_value.setEnabled(true);
                            ki_value.setEnabled(true);
                            kp_value.setEnabled(true);
                            mas_value.setEnabled(true);
                            kdspinner.setEnabled(true);
                            kispinner.setEnabled(true);
                            masspinner.setEnabled(true);
                            kpspinner.setEnabled(true);
                            auto_tuning = !auto_tuning;
                        } catch (IOException e) {
                            e.printStackTrace();
                            msg("Connection Error...");
                            isBtConnected.setBoo(false);
                            try_to_connect_again();


                        }
                    } else {
                        try {
                            btSocket.getOutputStream().write("tn0#".toString().getBytes());

                            autotuningbutton.setBackgroundColor(getResources().getColor(R.color.green));
                            autotuningbutton.setText("AUTOMATIC TUNING");
                            kd_value.setEnabled(false);
                            ki_value.setEnabled(false);
                            kp_value.setEnabled(false);
                            mas_value.setEnabled(false);

                            kp_value.setProgress(15);
                            ki_value.setProgress(190);
                            kd_value.setProgress((int) 0.6);
                            mas_value.setProgress(30);
                            selectSpinnerItemByValue(kpspinner, 15);
                            selectSpinnerItemByValue(kispinner, 190);
                            selectSpinnerItemByValue(kdspinner, 0.6);
                            selectSpinnerItemByValue(masspinner, 30);
                            kdspinner.setEnabled(false);
                            kispinner.setEnabled(false);
                            masspinner.setEnabled(false);
                            kpspinner.setEnabled(false);
                            auto_tuning = !auto_tuning;
                        } catch (IOException e) {
                            e.printStackTrace();
                            msg("Connection Error...");
                            isBtConnected.setBoo(false);
                            try_to_connect_again();


                        }
                    }
                }
            }
        });


        /*Seekbar changes*/


        kd_value.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    selectSpinnerItemByValue(kdspinner, progress);
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }

        });


        ki_value.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    selectSpinnerItemByValue(kispinner, progress);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });



            kp_value.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    if (fromUser) {
                        selectSpinnerItemByValue(kpspinner, progress);
                    }
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });


            mas_value.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    if (fromUser) {
                        selectSpinnerItemByValue(masspinner, progress);
                    }
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });



    }
    private class ConnectBT extends AsyncTask<Void, Void, Void>  // UI thread
    {
        private boolean ConnectSuccess = true; //if it's here, it's almost connected

        @Override
        protected void onPreExecute()
        {
            progress = ProgressDialog.show(MainActivity.this, "Connecting...", "Please wait!!!");  //show a progress dialog
        }

        @Override
        protected Void doInBackground(Void... devices) //while the progress dialog is shown, the connection is done in background
        {
            try
            {
                if (btSocket == null || !isBtConnected.isBoo())
                {
                    mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();//get the mobile bluetooth device
                    BluetoothDevice dispositivo = mBluetoothAdapter.getRemoteDevice(device_address);//connects to the device's address and checks if it's available
                    btSocket = dispositivo.createInsecureRfcommSocketToServiceRecord(myUUID);//create a RFCOMM (SPP) connection
                    BluetoothAdapter.getDefaultAdapter().cancelDiscovery();
                    btSocket.connect();//start connection
                }
            }
            catch (IOException e)
            {
                ConnectSuccess = false;//if the try failed, you can check the exception here
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void result) //after the doInBackground, it checks if everything went fine
        {
            super.onPostExecute(result);

            if (!ConnectSuccess)
            {
                msg("Robogo Bluetooth Connection Failed.Try again.");
            }
            else
            {
                msg("Connected.");
                isBtConnected.setBoo(true);
                reset_values();
            }
            progress.dismiss();
        }
    }
    private void msg(String s)
    {
        Toast.makeText(getApplicationContext(),s,Toast.LENGTH_SHORT).show();
    }
    public void try_to_connect_again(){

            new ConnectBT().execute();

    }
    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {

        getMenuInflater().inflate(R.menu.toolbar, menu);
        isBtConnected.setListener(new BooVariable.ChangeListener() {
            @Override
            public void onChange() {
                if (isBtConnected.isBoo()){
                    menu.getItem(0).setVisible(false);
                    menu.getItem(1).setVisible(true);
                    menu.getItem(2).setVisible(false);
                }
                else{
                    menu.getItem(0).setVisible(true);
                    menu.getItem(1).setVisible(false);
                    menu.getItem(2).setVisible(true);
                }
            }
        });
        if (isBtConnected.isBoo()){
            menu.getItem(0).setVisible(false);
            menu.getItem(1).setVisible(true);
            menu.getItem(2).setVisible(false);
        }
        else{
            menu.getItem(0).setVisible(true);
            menu.getItem(1).setVisible(false);
            menu.getItem(2).setVisible(true);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        int id = item.getItemId();
        //menü seçimi için switch case kullanıldı
        switch (id){
            case R.id.refreshconnection:
                //menu1 id sine sahip menü seçiminde yapılacak işlemler.
                try_to_connect_again();
                return true;
            case  R.id.information:
                LayoutInflater factory = LayoutInflater.from(MainActivity.this);
                final View view = factory.inflate(R.layout.infolayout, null);

                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

                builder.setView(view);




                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //Tamam butonuna basılınca yapılacaklar

                    }
                });


                builder.show();


                return true;

        }

        return false;
    }
    public void reset_values(){
        kp_value.setProgress(15);
        ki_value.setProgress(190);
        kd_value.setProgress((int) 0.6);
        mas_value.setProgress(30);
        selectSpinnerItemByValue(kpspinner, 15);
        selectSpinnerItemByValue(kispinner, 190);
        selectSpinnerItemByValue(kdspinner, 0.6);
        selectSpinnerItemByValue(masspinner, 30);
        kd_value.setEnabled(false);
        ki_value.setEnabled(false);
        kp_value.setEnabled(false);
        mas_value.setEnabled(false);
        kdspinner.setEnabled(false);
        kispinner.setEnabled(false);
        masspinner.setEnabled(false);
        kpspinner.setEnabled(false);
        autotuningbutton.setBackgroundColor(getResources().getColor(R.color.green));
        autotuningbutton.setText("AUTOMATIC TUNING");
        onOfButton.setBackgroundColor(getResources().getColor(R.color.green));
        on_off=true;
        auto_tuning=true;
    }

    public void selectSpinnerItemByValue(Spinner spnr, double value) {
        for (int i = 0; i < spnr.getCount(); i++) {
            if (Double.parseDouble(spnr.getItemAtPosition(i).toString())==value) {
                spnr.setSelection(i);
                break;
            }
        }
    }


}
